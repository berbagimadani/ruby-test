class OrganizationDetail < ApplicationRecord
	validates :name, presence:true 
	validates :phone, presence:true
	mount_uploader :avatar, ImageUploader
end
