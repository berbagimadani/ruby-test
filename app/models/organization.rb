class Organization < ApplicationRecord
	validates :name, presence:true
	validates :email, presence:true, uniqueness:true
	validates :phone, presence:true
	mount_uploader :logo, ImageUploader
	#has_many :organization_details
end
