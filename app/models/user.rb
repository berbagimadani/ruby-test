class User < ApplicationRecord
  rolify
  has_secure_password

  validates :email, presence:true, uniqueness:true
  #validates :password_confirmation, presence:true
  mount_uploader :avatar, ImageUploader
end
