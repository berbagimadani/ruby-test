json.extract! organization, :id, :name, :phone, :email, :website, :created_at, :updated_at
json.url organization_url(organization, format: :json)
