json.extract! organization_detail, :id, :name, :phone, :email, :created_at, :updated_at
json.url organization_detail_url(organization_detail, format: :json)
