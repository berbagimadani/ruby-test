class ApplicationController < ActionController::Base  
  before_action :authenticate_with_http_digest

  helper_method :current_user

  def current_user
    if session[:user_id]
      @current_user ||= User.find(session[:user_id])
    else
      @current_user = nil
    end
  end

  def signed_in_user!
  	if !session[:user_id]
  		redirect_to "/", notice: 'You dont have enough permissions to be here'
  	end
  end

end
