class CreateOrganizationDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :organization_details do |t| 
      t.string 	:name
      t.string 	:phone
      t.string 	:email
      t.references :organization, index: true, foreign_key: true
      t.string 	:avatar

      t.timestamps
    end
    add_index :organization_details, :name
    add_index :organization_details, :phone

  end
end
