class CreateOrganizations < ActiveRecord::Migration[5.2]
  def change
    create_table :organizations do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :website
      t.string :logo
      t.timestamps
    end
    add_index :organizations, :name
    add_index :organizations, :phone
    add_index :organizations, :email
  end
end
