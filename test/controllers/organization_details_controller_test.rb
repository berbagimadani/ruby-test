require 'test_helper'

class OrganizationDetailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @organization_detail = organization_details(:one)
  end

  test "should get index" do
    get organization_details_url
    assert_response :success
  end

  test "should get new" do
    get new_organization_detail_url
    assert_response :success
  end

  test "should create organization_detail" do
    assert_difference('OrganizationDetail.count') do
      post organization_details_url, params: { organization_detail: { email: @organization_detail.email, name: @organization_detail.name, phone: @organization_detail.phone, website: @organization_detail.website } }
    end

    assert_redirected_to organization_detail_url(OrganizationDetail.last)
  end

  test "should show organization_detail" do
    get organization_detail_url(@organization_detail)
    assert_response :success
  end

  test "should get edit" do
    get edit_organization_detail_url(@organization_detail)
    assert_response :success
  end

  test "should update organization_detail" do
    patch organization_detail_url(@organization_detail), params: { organization_detail: { email: @organization_detail.email, name: @organization_detail.name, phone: @organization_detail.phone, website: @organization_detail.website } }
    assert_redirected_to organization_detail_url(@organization_detail)
  end

  test "should destroy organization_detail" do
    assert_difference('OrganizationDetail.count', -1) do
      delete organization_detail_url(@organization_detail)
    end

    assert_redirected_to organization_details_url
  end
end
