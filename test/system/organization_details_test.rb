require "application_system_test_case"

class OrganizationDetailsTest < ApplicationSystemTestCase
  setup do
    @organization_detail = organization_details(:one)
  end

  test "visiting the index" do
    visit organization_details_url
    assert_selector "h1", text: "Organization Details"
  end

  test "creating a Organization detail" do
    visit organization_details_url
    click_on "New Organization Detail"

    fill_in "Email", with: @organization_detail.email
    fill_in "Name", with: @organization_detail.name
    fill_in "Phone", with: @organization_detail.phone
    fill_in "Website", with: @organization_detail.website
    click_on "Create Organization detail"

    assert_text "Organization detail was successfully created"
    click_on "Back"
  end

  test "updating a Organization detail" do
    visit organization_details_url
    click_on "Edit", match: :first

    fill_in "Email", with: @organization_detail.email
    fill_in "Name", with: @organization_detail.name
    fill_in "Phone", with: @organization_detail.phone
    fill_in "Website", with: @organization_detail.website
    click_on "Update Organization detail"

    assert_text "Organization detail was successfully updated"
    click_on "Back"
  end

  test "destroying a Organization detail" do
    visit organization_details_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Organization detail was successfully destroyed"
  end
end
